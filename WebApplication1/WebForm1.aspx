﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="WebApplication1.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Volunteer Registeration Form</title>
</head>
<body>
    <form id="form1" runat="server">
        <div><h1>Volunteer Registeration</h1>
        </div>
        <div>
            <label><b>Name</b></label>
                <asp:TextBox runat="server" ID="fname" placeholder="First and Last name"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" id="validatename" ErrorMessage="Enter valid name" ControlToValidate="fname"></asp:RequiredFieldValidator>
        </div>
   
        <div>
            <label><b>Contact</b></label>
            <asp:TextBox runat="server" ID="numb" placeholder="10 digit phone number"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ID="contc" ErrorMessage="Enter Phone Number" ControlToValidate="numb" ></asp:RequiredFieldValidator>
            <asp:CompareValidator runat="server" ID="conta" ErrorMessage="Not a valid number" Type="String" Operator="Equal" ControlToValidate="numb" ValueToCompare="9999999999" ></asp:CompareValidator>
        </div>

        <div>
                <label><b>Email</b></label>
                <asp:TextBox runat="server" ID="mail" placeholder="example@website.com" ></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="mail" ErrorMessage="Email address Required"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator runat="server" ID="regValidEmail" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="mail" ErrorMessage="Please enter valid email ID" ></asp:RegularExpressionValidator>
        </div>

        <div id="available" runat="server">
            <label><b>Availability</b></label>
            <asp:CheckBox runat="server" ID="weekday1" Text="mon-fri" />
            <asp:CheckBox runat="server" ID="weekend1" Text="sat-sun" />
                        
        </div>
        <div id="time" runat="server">
            <label><b>What time can you work?</b></label>
            <div>
            <asp:RadioButton runat="server" ID="anytime" Text="anytime" GroupName="timea" />
            <asp:RadioButton runat="server" ID="time1" Text="9-12" GroupName="timea" />
            <asp:RadioButton runat="server" ID="time2" Text="1-5" GroupName="timea" />
            <asp:RadioButton runat="server" ID="night" Text="All night" GroupName="timea" />
            </div>
        </div>
        <div id="position" runat="server">
            <label><b>Position </b></label>
                <asp:DropDownList runat="server" ID="positn">
                    <asp:ListItem Value="studnt" Text="Student"></asp:ListItem>
                    <asp:listitem Value="job" Text="Job"></asp:listitem>
                    <asp:ListItem Value="self" Text="Self-employed"></asp:ListItem>
                    <asp:ListItem Value="unemp" Text="unemployed"></asp:ListItem>
                </asp:DropDownList>
        </div>
        <div id="talent" runat="server">
            <label><b>Any special talent?</b></label>
            <div>
                <asp:Checkbox runat="server" ID="graphic" Text="Graphic Designer" /> <br />
                <asp:CheckBox runat="server" ID="deco" Text="Decoration" /> <br />
                <asp:CheckBox runat="server" ID="it" Text="Computer Tech." /> <br />
                <asp:CheckBox runat="server" ID="perfo" Text="Stage Performer" /> <br />
            </div>
        </div>

        <div>
            <label><b>Preffered way of communication?</b></label>
            <asp:RadioButton runat="server" ID="emal" Text="Email" GroupName="com" />
            <asp:RadioButton runat="server" ID="phn" Text="Call" GroupName="com" />
        </div>

        <div>
            <asp:Button runat="server" ID="mybutton" Text="Submit" />
        </div>
        <asp:ValidationSummary ID="ValidationSummary1" runat="server" ForeColor="Red" />
    </form>
</body>
</html>
